let value;
let state = true;
const arr = [];

while (state) {
  value = +prompt("Write a number");

  if (!Number.isNaN(value) && value % 1 === 0) {
    state = false;
  }
}

for (let i = 1; i <= value; i++) {
  if (i % 5 === 0) {
    arr.push(i);
  }
}

if (arr.length === 0) {
  console.log("Sorry, no numbers");
} else {
  console.log(arr.join(" "));
}
